/**
*  vim:set sw=2 ts=2 sts=2 ft=javascript expandtab:
*
*  # Authentification Module
*
*  ## License
*
*  Licensed to the Apache Software Foundation (ASF) under one
*  or more contributor license agreements.  See the NOTICE file
*  distributed with this work for additional information
*  regarding copyright ownership.  The ASF licenses this file
*  to you under the Apache License, Version 2.0 (the
*  "License"); you may not use this file except in compliance
*  with the License.  You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing,
*  software distributed under the License is distributed on an
*  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
*  KIND, either express or implied.  See the License for the
*  specific language governing permissions and limitations
*  under the License.
*
*  ## Description
*
*  This module contains all functions about authentification of MyPads users.
*  It's mainly based upon the excellent *passport* and *jsonwebtoken* Node
*  libraries.
*
*  TODO: abstract passport.authenticate to allow more easily custom errors
*/


// External dependencies
var ld         = require('lodash');
var jwt        = require('jsonwebtoken');
var ExtractJwt = require('passport-jwt').ExtractJwt;
var KeycloakStrategy = require('@exlinc/keycloak-passport');
var settings;
try {
  // Normal case : when installed as a plugin
  settings = require('ep_etherpad-lite/node/utils/Settings');
}
catch (e) {
  if (process.env.TEST_LDAP) {
    settings = {
      'users': {
        'admin': {
          'password': 'admin',
          'is_admin': true
        },
        'parker': {
          'password': 'lovesKubiak',
          'is_admin': false
        }
      },
      'ep_mypads': {
        'ldap': {
          'url': 'ldap://rroemhild-test-openldap',
          'bindDN': 'cn=admin,dc=planetexpress,dc=com',
          'bindCredentials': 'GoodNewsEveryone',
          'searchBase': 'ou=people,dc=planetexpress,dc=com',
          'searchFilter': '(uid={{username}})',
          'properties': {
            'login': 'uid',
            'email': 'mail',
            'firstname': 'givenName',
            'lastname': 'sn'
          },
          'defaultLang': 'fr'
        }
      }
    };
  } else {
    // Testing case : we need to mock the express dependency
    settings = {
      users: {
        admin: { password: 'admin', is_admin: true },
        grace: { password: 'admin', is_admin: true },
        parker: { password: 'lovesKubiak', is_admin: false }
      }
    };
  }
}
var passport    = require('passport');
var JWTStrategy = require('passport-jwt').Strategy;
var cuid        = require('cuid');
// Local dependencies
var common = require('./model/common.js');
var user   = require('./model/user.js');
var conf   = require('./configuration.js');

var NOT_INTERNAL_AUTH_PWD = 'soooooo_useless';

module.exports = (function () {
  'use strict';

  /**
  * ## Authentification
  *
  * - `tokens` holds all actives tokens with user logins as key and user data
  *   as value, plus a special `key` attribute to check validity
  * - `secret` is the temporary random string key. It will be reinitialized
  *   after server relaunch, invaliditing all active users
  */

  var auth = {
    tokens: {},
    adminTokens: {},
    secret: cuid() // secret per etherpad session
  };

  /**
  * ## Internal functions
  *
  * These functions are not private like with closures, for testing purposes,
  * but they are expected be used only internally by other MyPads functions.
  */

  auth.fn = {};

  /**
  * ### getUser
  *
  * `getUser` is a synchronous function that checks if the given encrypted
  * `token` is valid, ie if login has been already found in local cache and if
  * the given key is the same as the generated one.
  * It returns the *user* object in case of success and *false* otherwise.
  *
  * TODO: unit test missing
  */

  auth.fn.getUser = function (token) {
    var jwt_payload = jwt.decode(token, auth.secret);
    if (!jwt_payload) { return false; }
    var login    = jwt_payload.login;
    var userAuth = (login && auth.tokens[login]);
    if (userAuth && (auth.tokens[login].key === jwt_payload.key)) {
      return auth.tokens[login];
    } else {
      return false;
    }
  };

  /**
  * ### local
  *
  * `local` is a synchronous function used to set up JWT strategy.
  */

  auth.fn.local = function () {
    passport.use(
      "keycloak",
      new KeycloakStrategy(
        {
          host: process.env.KEYCLOAK_HOST,
          realm: process.env.KEYCLOAK_REALM,
          clientID: process.env.KEYCLOAK_CLIENT_ID,
          clientSecret: process.env.KEYCLOAK_CLIENT_SECRET,
          callbackURL: `/api${AUTH_KEYCLOAK_CALLBACK}`
        },
        (accessToken, refreshToken, profile, done) => {
          // This is called after a successful authentication has been completed
          // Here's a sample of what you can then do, i.e., write the user to your DB
          user.set({
            email: profile.email,
            login: profile.username
            },(err) => {
            assert.ifError(err);
            }
          );
        }
      )
    );
  };

  /**
  * ### checkMyPadsUser
  *
  * `checkMyPadsUser` checks user existence from given `login` and `pass`. It
  * uses the last argument, `callback` function, to return an *error* or *null*
  * and the *user* object.
  */

  auth.fn.checkMyPadsUser = function (login, callback) {
    user.get(login.login, function(err, u) {
      // If the user does not exist, we create the user
      if (err) {
        user.set(login, callback);
      } else {
        return callback(null, u);
      }
    });
  };

  /**
  * ### checkAdminUser
  *
  * `checkAdminUser` checks admin existence from given `login` and `pass`. It
  * uses the last argument, `callback` function, to return an *error* or *null*
  * and the *user* object.
  */

  auth.fn.checkAdminUser = function (login, pass, callback) {
    if (!settings || !settings.users || !settings.users[login]) {
      return callback(new Error('BACKEND.ERROR.USER.NOT_FOUND'), null);
    }
    var u   = settings.users[login];
    u.login = login;
    var emsg;
    if (pass !== u.password) {
      emsg = 'BACKEND.ERROR.AUTHENTICATION.PASSWORD_INCORRECT';
      return callback(new Error(emsg, false));
    }
    if (!u.is_admin) {
      emsg = 'BACKEND.ERROR.AUTHENTICATION.ADMIN';
      return callback(new Error(emsg, false));
    }
    return callback(null, u);
  };

  /**
  * ### localFn
  *
  * `localFn` is a function that checks if login and password are correct. It
  * takes :
  *
  * - a `login` string
  * - a `password` string
  * - a `callback` function, returning
  *   - *Error* if there is a problem
  *   - *null*, *false* and an object for auth error
  *   - *null* and the *user* object for auth success
  */

  auth.fn.localFn = function (login, callback) {
    user.get(login, function (err, u) {
      if (err) { return callback(err); }
      callback(null, u);
    });
  };

  /**
  * ## Public functions
  *
  * ### init
  *
  * `init` is a synchronous function used to set up authentification. It :
  *
  * - initializes local strategy by default
  * - uses of passport middlwares for express
  * - launch session middleware bundled with express, using secret phrase saved
  *   in database
  * - mocks admin behavior if in testingMode
  */

  auth.init = function (app) {
    app.use(passport.initialize());
    auth.fn.local();
  };

  return auth;

}).call(this);
